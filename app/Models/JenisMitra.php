<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisMitra extends Model
{
    protected $table = 'jenis_mitra';

    public $timestamps = false;
    
    protected $fillable = [
        'nama',
    ];
}
